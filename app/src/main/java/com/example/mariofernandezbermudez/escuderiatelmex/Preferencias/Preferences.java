package com.example.mariofernandezbermudez.escuderiatelmex.Preferencias;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.mariofernandezbermudez.escuderiatelmex.Data.ResponseGetNews;
import com.example.mariofernandezbermudez.escuderiatelmex.Net.Language;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

public class Preferences  {
    private static final String TAG = Preferences.class.getSimpleName();
    public Context              context;
    public SharedPreferences    pref;

    public Preferences(Context context){
        this.context = context;
        pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
    }


    public void saveNews(ResponseGetNews news, String tag){
        try{
            SharedPreferences.Editor editor = pref.edit();
            Gson gson = new Gson();
            editor.putString(tag,  gson.toJson(news));
            editor.commit();
            Log.d(TAG, "saveNews() called");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getNews(Language language){
        final String TAG_PREFERENCES;
        if(language.equals(Language.English)){
            TAG_PREFERENCES     = "newsEnglish";
        }else{
            TAG_PREFERENCES     = "newsEspanish";
        }

        SharedPreferences.Editor editor = pref.edit();
        Gson gson = new Gson();
        return pref.getString(TAG_PREFERENCES, null);
    }

}
