package com.example.mariofernandezbermudez.escuderiatelmex.Net;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mariofernandezbermudez.escuderiatelmex.Data.ResponseGetNews;
import com.example.mariofernandezbermudez.escuderiatelmex.Preferencias.Preferences;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class Connection {
    final String    url="http://webservices.escuderiatelmex.com/ET_MX_APP_WS/services/EscuderiaTelmexWS/getNews?lastUpdate=0000-00-00%2000:00&lang=";
    final String    url_Complete="lastUpdate=0000-00-00%2000:00&lang=es";
    Context         context;
    RequestQueue    queue ;
    ResponseGetNews ResponseGetNews;
    String          url_request;
    Preferences     preferences;

    public Connection(Context contexto){
        context =contexto;
        queue = Volley.newRequestQueue(context);
        preferences = new Preferences(context);
    }

    public ResponseGetNews getRequest(Language language){
        final String TAG_PREFERENCES;
        if(language.equals(Language.English)){
            url_request         = url.concat("en");
            TAG_PREFERENCES     = "newsEnglish";
        }else{
            url_request = url.concat("es");
            TAG_PREFERENCES     = "newsEspanish";
        }
        Log.i("Response", "===========>>>>> url_request  "+url_request);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url_request, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Log.i("Response", "===========>>>>> response from getRequest:  "+response.toString());
                            String responseJson =response.toString();
                            Gson gson = new Gson();
                            ResponseGetNews = gson.fromJson(responseJson, ResponseGetNews.class);
                            preferences.saveNews(ResponseGetNews, TAG_PREFERENCES);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error.Response", "===========>>>>> response: "+error.toString());
                    }
                }
        );
        queue.add(getRequest);
        return ResponseGetNews;
    }

}
