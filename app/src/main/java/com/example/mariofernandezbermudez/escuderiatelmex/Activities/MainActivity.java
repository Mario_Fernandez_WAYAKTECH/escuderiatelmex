package com.example.mariofernandezbermudez.escuderiatelmex.Activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.mariofernandezbermudez.escuderiatelmex.Data.ResponseGetNews;
import com.example.mariofernandezbermudez.escuderiatelmex.Net.Connection;
import com.example.mariofernandezbermudez.escuderiatelmex.Net.Language;
import com.example.mariofernandezbermudez.escuderiatelmex.R;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Connection connection;
    ResponseGetNews responseEnglish, responseEspanish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connection = new Connection(MainActivity.this);
        getNews();
    }

    public void getNews(){
        try{
            responseEnglish = connection.getRequest(Language.English);
            responseEspanish = connection.getRequest(Language.Espanish);
            Log.d(TAG, "getNews() called");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void saveNews(ResponseGetNews news,  String tag){
        try{
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            Gson gson = new Gson();
            editor.putString(tag,  gson.toJson(news));
            editor.commit();
            Log.d(TAG, "saveNews() called");
        }catch(Exception e){
            e.printStackTrace();
        }
    }



}
