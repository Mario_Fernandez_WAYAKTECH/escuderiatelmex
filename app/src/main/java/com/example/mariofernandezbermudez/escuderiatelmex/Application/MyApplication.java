package com.example.mariofernandezbermudez.escuderiatelmex.Application;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

public class MyApplication extends Application{
    private static MyApplication myApplication;



    public static Context getAppContext() {
        return myApplication;
    }
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        myApplication = this;
    }
}
