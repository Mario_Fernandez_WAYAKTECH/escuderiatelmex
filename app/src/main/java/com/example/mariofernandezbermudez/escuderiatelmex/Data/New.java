package com.example.mariofernandezbermudez.escuderiatelmex.Data;

public class New {
    private int         id;
    private String      creationDate;
    private String      status;
    private String      urlImagen;
    private String      urlImagenHD;
    private String      title;
    private String      description;
    private String      text;
    private String[]    races;
    private String[]    galleries;
    private String[]    drivers;
    private int[]       categories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getUrlImagenHD() {
        return urlImagenHD;
    }

    public void setUrlImagenHD(String urlImagenHD) {
        this.urlImagenHD = urlImagenHD;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getRaces() {
        return races;
    }

    public void setRaces(String[] races) {
        this.races = races;
    }

    public String[] getGalleries() {
        return galleries;
    }

    public void setGalleries(String[] galleries) {
        this.galleries = galleries;
    }

    public String[] getDrivers() {
        return drivers;
    }

    public void setDrivers(String[] drivers) {
        this.drivers = drivers;
    }

    public int[] getCategories() {
        return categories;
    }

    public void setCategories(int[] categories) {
        this.categories = categories;
    }
}
