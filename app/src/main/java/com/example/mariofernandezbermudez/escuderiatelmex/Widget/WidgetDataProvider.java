package com.example.mariofernandezbermudez.escuderiatelmex.Widget;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.mariofernandezbermudez.escuderiatelmex.Data.New;
import com.example.mariofernandezbermudez.escuderiatelmex.Data.ResponseGetNews;
import com.example.mariofernandezbermudez.escuderiatelmex.Net.Language;
import com.example.mariofernandezbermudez.escuderiatelmex.Preferencias.Preferences;
import com.example.mariofernandezbermudez.escuderiatelmex.R;
import com.google.gson.Gson;

import java.util.List;

public class WidgetDataProvider implements RemoteViewsService.RemoteViewsFactory {
    private static final String TAG = "WidgetDataProvider";
    List<New> news ;
    Context mContext = null;


    public WidgetDataProvider(Context context, Intent intent) {
        mContext = context;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate() called");
        initData();
    }

    @Override
    public void onDataSetChanged() {
        Log.d(TAG, "onDataSetChanged() called");
        initData();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount() called  "+news.size());
        return news.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews view = new RemoteViews(mContext.getPackageName(), R.layout.element_news);
        ImageView imageView ;
        view.setTextViewText(R.id.tv_news, "Se actualiza el texto");
        return view;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    private void initData() {
        try{
            news.clear();
            Preferences pref = new Preferences(mContext);
            String newsString = pref.getNews(Language.Espanish);
            Gson gson = new Gson();
            ResponseGetNews newsSaved = gson.fromJson(newsString, ResponseGetNews.class);
            news = newsSaved.getNews();
            Log.d(TAG, "initData() called  "+news.size());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
